# This feature should search over school name, city name, and state name.
# The top 3 matching results should be returned.

from utils import read_csv, timeit


def pre_process_file() -> dict:
    file = read_csv()
    school_map = {}
    city_column = 'LCITY05'
    state_column = 'LSTATE05'
    school_column = 'SCHNAM05'

    for row in file:
        school, city, state = row[school_column], row[city_column], row[
            state_column]
        key = f'{school} {city} {state}'.upper()
        school_map[key] = [school, city, state]

    return school_map


def similarity_ratio(search_keys: list[str], school_search_key: str) -> int:
    ratio = 0
    weights = len(search_keys)

    for word in search_keys:
        if word in school_search_key:
            ratio = (ratio + len(word)) * weights
        weights -= 1

    return ratio


def create_search_match_ratio_map(query: str,
                                  schools_map: dict) -> dict[int, list[str]]:
    search_keys = query.split(" ")
    search_ration_map = {}
    for school_search_key, school_dict in schools_map.items():
        ratio = similarity_ratio(search_keys, school_search_key)

        if not ratio:
            continue
        if ratio in search_ration_map:
            search_ration_map[ratio].append(school_search_key)
        else:
            search_ration_map[ratio] = [school_search_key]

    return search_ration_map


@timeit
def query_schools(query: str, schools_map: dict) -> None:
    number_of_records = 3
    query = query.upper()
    search_ratio_map = create_search_match_ratio_map(query, schools_map)

    sorted_ratio = sorted(search_ratio_map.keys(), reverse=True)[:3]

    count = 1
    for index in range(len(sorted_ratio)):
        keys = search_ratio_map[sorted_ratio[index]]
        for search_key in keys:
            school, city, state = schools_map[search_key]
            print(f'{count}. {school}, {city}, {state}')

            count += 1
            if count > number_of_records:
                break

        if count > number_of_records:
            break


def search_schools(query: str) -> None:
    schools_map = pre_process_file()
    query_schools(query, schools_map)


if __name__ == "__main__":
    search_schools('elementary school highland park')
    search_schools('NEW PINE CREEK')
    search_schools('jefferson belleville')
    search_schools('riverside school 44')
    search_schools('')
    search_schools('CHALKVILLE')
