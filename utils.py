import csv
import time
from functools import wraps


def timeit(func):
    @wraps(func)
    def timeit_wrapper(*args, **kwargs):
        start = time.perf_counter()
        result = func(*args, **kwargs)
        end = time.perf_counter()
        print(f"Elapsed time: {end - start}s")
        return result

    return timeit_wrapper


def read_csv():
    data = csv.DictReader(open("school_data.csv", encoding='iso-8859-1'))
    return data
