import operator
from collections import Counter

from utils import read_csv


def total_schools() -> int:
    file = read_csv()
    count = len(list(file))
    return count


def schools_by_value(value: str) -> list:
    file = read_csv()
    counts = Counter(map(operator.itemgetter(value), file)).items()
    return sorted(counts)


def unique_cities_with_schools() -> set:
    file = read_csv()
    cities = set(map(operator.itemgetter('LCITY05'), file))
    return cities


def print_counts():
    city_column = 'LCITY05'
    state_column = 'LSTATE05'
    metro_locale_column = 'MLOCALE'

    total_count = total_schools()
    print(f'Total Schools: {total_count}')

    schools_by_state = schools_by_value(state_column)
    print(f'Schools by state')
    for state, count in schools_by_state:
        print(f'{state}: {count}')

    schools_by_metro_centric = schools_by_value(metro_locale_column)
    print(f'Schools by Metro-centric locale')
    for locale, count in schools_by_metro_centric:
        print(f'{locale}: {count}')

    schools_by_city = schools_by_value(city_column)
    schools_by_city = sorted(schools_by_city, key=lambda x: x[1], reverse=True)
    first_city, number_of_schools = schools_by_city[0]
    print(f'City with most schools {first_city} ({number_of_schools})')

    cities = unique_cities_with_schools()
    print(f'Unique cities with schools: {len(cities)}')


if __name__ == "__main__":
    # of course is not necessary read the file in each function,
    # but I kept this way to all of them be self-contained
    print_counts()
